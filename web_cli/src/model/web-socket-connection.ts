import { plainToInstance } from "class-transformer";
import type { Writable } from "svelte/store";
import { WEBSOCKET_BASE_URL } from "../config";
import { AddUserToRoomEvent } from "../events/add-user-to-room";
import { ReturnAddVectorToUserEvent } from "../events/add-vector-to-user/return-add-vector-to-user";
import type { BaseEvent } from "../events/base";
import { RemoveUserFromRoom } from "../events/remove-user-from-room";
import { ReturnRemoveVectorFromuser } from "../events/remove-vector-from-user/return-remove-vector-from-user";
import { ReturnUserNotFoundEvent } from "../events/return-user-not-found";
import { ReturnAddUserToRoomEvent } from "../events/return/return-add-user-to-room";
import type { ReturnBaseEvent } from "../events/return/return-base";
import { ReturnInitialRoomEvent } from "../events/return/return-initial-room";
import { ReturnRemoveUserFromRoom as ReturnRemoveUserFromRoomEvent } from "../events/return/return-remove-user-from-room";
import { EventType } from "../events/type";
import type { Room, User } from "./room";

// @ts-ignore
const MAPPING: Record<EventType, any> = {
  [EventType.ADD_USER_TO_ROOM]: AddUserToRoomEvent,
  [EventType.REMOVE_USER_FROM_ROOM]: RemoveUserFromRoom,

  [EventType.RETURN_INITIAL_ROOM_STATE]: ReturnInitialRoomEvent,
  [EventType.RETURN_USER_NOT_FOUND]: ReturnUserNotFoundEvent,

  [EventType.RETURN_NEW_USER]: ReturnAddUserToRoomEvent,
  [EventType.RETURN_REMOVE_USER_FROM_ROOM]: ReturnRemoveUserFromRoomEvent,

  [EventType.RETURN_ADD_VECTOR_TO_USER]: ReturnAddVectorToUserEvent,

  [EventType.RETURN_REMOVE_VECTOR_FROM_USER]: ReturnRemoveVectorFromuser
};

export class WebSocketConnection extends WebSocket {
  private room: Writable<Room>;
  private selectedUser: Writable<User>;

  constructor(
    roomId: string,
    protocols: string | string[] = "",
    room: Writable<Room>,
    selectedUser: Writable<User>
  ) {
    const url = `${WEBSOCKET_BASE_URL}/room/${roomId}/ws`;
    super(url, protocols);
    this.room = room;
    this.selectedUser = selectedUser;
    this.onmessage = this.handleEvent;
  }

  sendMessage<Message extends BaseEvent>(data: Message) {
    console.log("Send data:", data);
    this.send(JSON.stringify(data));
  }

  private handleEvent(rawEvent: MessageEvent) {
    const parsedData = JSON.parse(rawEvent.data);
    if (!parsedData["event_type"]) {
      throw new Error("No eventType found!");
    }
    // https://stackoverflow.com/a/56076148
    const eventType: EventType =
      EventType[parsedData["event_type"] as string as keyof typeof EventType];

    if (!eventType) {
      throw new Error(`Event cannot be parsed: ${parsedData["event_type"]}`);
    }

    if (eventType in MAPPING) {
      const event = plainToInstance<ReturnBaseEvent, unknown>(
        MAPPING[eventType],
        parsedData
      ) as unknown as ReturnBaseEvent;
      const processedRoom = event.process(this.room, this.selectedUser);
    } else {
      throw new Error(`EventType ${eventType} does not exists!`);
    }
  }
}
