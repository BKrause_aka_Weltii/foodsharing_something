import { BACKEND_URL } from "../config";

export class BackendService {
  static async createNewRoom(roomName: string): Promise<string> {
    const response = await fetch(`${BACKEND_URL}/room`, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify({
        name: roomName,
      }),
    });
    if (response.status >= 400) {
      return Promise.reject(response.body);
    }
    const json = await response.json();
    return Promise.resolve(json["id"]);
  }
}
