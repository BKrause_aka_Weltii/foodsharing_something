export interface Vector {
  x: number;
  y: number;
}

export interface User {
  username: string;
  color: string;
  vectors: Vector[];
}

export interface Room {
  name: string;
  id: string;
  users: Record<string, User>;
}
