import type { EventType } from "./type";

export abstract class BaseEvent {
  protected eventType: EventType;
  private roomId: string;

  protected constructor(eventType: EventType, roomId: string) {
    this.eventType = eventType;
    this.roomId = roomId;
  }

  public getEventType() {
    return this.eventType;
  }
}
