import type { Vector } from "../../model/room";
import { BaseEvent } from "../base";
import { EventType } from "../type";

export class RemoveVectorFromUserEvent extends BaseEvent {
  private username: string;
  private vector: Vector;

  constructor(roomId: string, username: string, vector: Vector) {
    super(EventType.REMOVE_VECTOR_FROM_USER, roomId);
    this.username = username;
    this.vector = vector;
  }
}
