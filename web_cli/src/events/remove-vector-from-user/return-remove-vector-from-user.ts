import type { Writable } from "svelte/store";
import type { Room, Vector } from "../../model/room";
import { ReturnBaseEvent } from "../return/return-base";
import { EventType } from "../type";

export class ReturnRemoveVectorFromuser extends ReturnBaseEvent {
  private username: string;
  private vector: Vector;

  constructor(username: string, vector: Vector) {
    super(EventType.RETURN_REMOVE_VECTOR_FROM_USER);
    this.username = username;
    this.vector = vector;
  }

  process(room: Writable<Room>): void {
    room.update((room) => {
      const user = room.users[this.username];
      user.vectors = user.vectors.filter((vector) => {
        return vector.x !== this.vector.x && vector.y !== this.vector.y;
      });
      return room;
    });
  }
}
