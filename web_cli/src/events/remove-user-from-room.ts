import { BaseEvent } from "./base";
import { EventType } from "./type";

export class RemoveUserFromRoom extends BaseEvent {
  private username: string;

  constructor(roomId: string, username: string) {
    super(EventType.REMOVE_USER_FROM_ROOM, roomId);
    this.username = username;
  }
}
