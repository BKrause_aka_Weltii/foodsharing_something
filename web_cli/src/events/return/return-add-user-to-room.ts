import { ReturnBaseEvent } from "./return-base";
import { EventType } from "../type";
import type { Writable } from "svelte/store";
import type { Room, User } from "../../model/room";

export class ReturnAddUserToRoomEvent extends ReturnBaseEvent {
  private user: User;

  constructor(user: User) {
    super(EventType.RETURN_NEW_USER);
    this.user = user;
  }

  process(room: Writable<Room>): void {
    room.update((room) => {
      room.users[this.user.username] = this.user;
      return room;
    });
  }
}
