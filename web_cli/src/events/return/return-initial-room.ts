import { ReturnBaseEvent } from "./return-base";
import { EventType } from "../type";
import type { Writable } from "svelte/store";
import type { Room } from "../../model/room";

export class ReturnInitialRoomEvent extends ReturnBaseEvent {
  private readonly room: Room;

  constructor(room: Room) {
    super(EventType.RETURN_INITIAL_ROOM_STATE);
    this.room = room;
  }

  process(room: Writable<Room>): void {
    room.set(this.room);
  }
}
