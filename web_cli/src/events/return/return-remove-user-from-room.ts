import { get, Writable } from "svelte/store";
import type { Room, User } from "../../model/room";
import { EventType } from "../type";
import { ReturnBaseEvent } from "./return-base";

export class ReturnRemoveUserFromRoom extends ReturnBaseEvent {
  private username: string;

  constructor(username: string) {
    super(EventType.RETURN_REMOVE_USER_FROM_ROOM);
    this.username = username;
  }

  process(room: Writable<Room>, selectedUser: Writable<User>): void {
    room.update((room) => {
      if (selectedUser) {
        const selUser = get(selectedUser);
        if (selUser && selUser.username === this.username) {
          selectedUser.set(null);
        }
      }
      delete room.users[this.username];
      return room;
    });
  }
}
