import type { Writable } from "svelte/store";
import type { Room, User } from "../../model/room";
import type { EventType } from "../type";

export abstract class ReturnBaseEvent {
  protected type: EventType;

  protected constructor(type: EventType) {
    this.type = type;
  }

  public getType() {
    return this.type;
  }

  /**
   * Process this event on the passed room.
   * The passed room can be modified by this event and must returned at the end!
   *
   * @param room Room to manipulate
   */
  abstract process(room: Writable<Room>, selectedUser: Writable<User>): void;
}
