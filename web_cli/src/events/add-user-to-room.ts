import { BaseEvent } from "./base";
import { EventType } from "./type";

export class AddUserToRoomEvent extends BaseEvent {
  private username: string;

  constructor(roomId: string, username: string) {
    super(EventType.ADD_USER_TO_ROOM, roomId);
    this.username = username;
  }
}
