import type { Writable } from "svelte/store";
import type { Room, Vector } from "../../model/room";
import { ReturnBaseEvent } from "../return/return-base";
import { EventType } from "../type";

export class ReturnAddVectorToUserEvent extends ReturnBaseEvent {
  private username: string;
  private vector: Vector;

  constructor(username: string, vector: Vector) {
    super(EventType.RETURN_ADD_VECTOR_TO_USER);
    this.username = username;
    this.vector = vector;
  }

  process(room: Writable<Room>): void {
    room.update((room) => {
      const user = room.users[this.username];
      user.vectors.push(this.vector);
      return room;
    });
  }
}
