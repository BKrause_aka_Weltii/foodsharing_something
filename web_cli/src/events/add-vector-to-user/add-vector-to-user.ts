import type { Vector } from "../../model/room";
import { BaseEvent } from "../base";
import { EventType } from "../type";

export class AddVectorToUserEvent extends BaseEvent {
  private username: string;
  private vector: Vector;

  constructor(roomId: string, username: string, vector: Vector) {
    super(EventType.ADD_VECTOR_TO_USER, roomId);
    this.username = username;
    this.vector = vector;
  }
}
