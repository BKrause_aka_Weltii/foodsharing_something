import type { Writable } from "svelte/store";
import type { Room, User } from "../model/room";
import { ReturnBaseEvent } from "./return/return-base";
import { EventType } from "./type";

export class ReturnUserNotFoundEvent extends ReturnBaseEvent {
  private username: string;

  constructor(username: string) {
    super(EventType.RETURN_USER_NOT_FOUND);
    this.username = username;
  }

  process(): void {}
}
