// @ts-ignore
const envs = window.ENV;

export const BASE_URL: string = envs.BASE_URL;
export const WEBSOCKET_BASE_URL: string = envs.WEBSOCKET_BASE_URL;
export const BACKEND_URL: string = envs.BACKEND_URL;