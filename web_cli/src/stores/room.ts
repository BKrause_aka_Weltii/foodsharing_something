import { writable } from "svelte/store";
import type { Room } from "../model/room";

export const room = writable<Room>({
  name: "Not loaded yet",
  id: "",
  users: {},
});
