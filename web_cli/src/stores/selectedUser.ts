import { writable } from "svelte/store";
import type { User } from "../model/room";

export const selectedUser = writable<User>();

let selectedUserInstance: User;

selectedUser.subscribe((user) => {
  selectedUserInstance = user;
});

export function isSelected(other: User) {
  if (selectedUserInstance) {
    return selectedUserInstance.username === other.username;
  }
}
