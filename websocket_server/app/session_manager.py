from typing import List

from fastapi import WebSocket

from app.events.base import BaseEvent
from app.room import Room
from app.room_factory import RoomFactory


class RoomSession:
    def __init__(self, room_id: str):
        self.room_id = room_id
        self.connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        print(f"Connect new websocket")
        self.connections.append(websocket)

    async def disconnect(self, websocket: WebSocket):
        if websocket in self.connections:
            self.connections.remove(websocket)
            await websocket.close()

    async def broadcast(self, event: BaseEvent):
        for connection in self.connections:
            await connection.send_json(event.dict())


class RoomNotAvailable(BaseException):
    def __init__(self, room_id: str):
        self.room_id = room_id


class SessionManager:
    def __init__(self, room_factory: RoomFactory):
        self.room_factory = room_factory
        self.sessions: List[RoomSession] = []

    def get_room_session(self, room_id: str) -> RoomSession:
        room_exists = self.room_factory.room_exists(room_id)
        if room_exists:
            for session in self.sessions:
                if session.room_id == room_id:
                    return session
            session = RoomSession(room_id)
            self.sessions.append(session)
            return session

        raise RoomNotAvailable(room_id)
