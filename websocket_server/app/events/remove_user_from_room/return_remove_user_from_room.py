from app.events.base import BaseEvent
from app.events.events import EventType


class ReturnUserFromRoomEvent(BaseEvent):
    event_type = EventType.RETURN_REMOVE_USER_FROM_ROOM
    broadcast = True
    username: str
