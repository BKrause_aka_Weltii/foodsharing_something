from app.events.events import EventType
from app.events.remove_user_from_room.return_remove_user_from_room import ReturnUserFromRoomEvent
from app.events.room_processable import RoomProcessableEvent
from app.room import Room
from app.room_factory import RoomFactory


class RemoveUserFromRoomEvent(RoomProcessableEvent):
    event_type = EventType.REMOVE_USER_FROM_ROOM
    username: str

    def process_room(self, room: Room, room_factory: RoomFactory):
        room.remove_user(self.username)

        room_factory.update_room(room)
        return ReturnUserFromRoomEvent(username=self.username)
