from app.events.base import BaseEvent
from app.events.events import EventType


class ReturnUserNotFound(BaseEvent):
    event_type = EventType.RETURN_USER_NOT_FOUND
    username: str
