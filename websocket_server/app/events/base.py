from __future__ import (
    annotations,
)

from typing import Optional

from pydantic import validator

from app.camel_model import CamelModel
from app.events.events import EventType
from app.room_factory import RoomFactory


class BaseEvent(CamelModel):
    event_type: EventType
    broadcast: bool = False

    class Config:
        use_enum_values = True

    def handle(self, room_factory: RoomFactory) -> Optional[BaseEvent]:
        pass

    @validator("event_type")
    def validate_event_type(cls, raw_event_type: str):
        return EventType(raw_event_type)

    def is_type(self, type: EventType):
        return self.event_type == type

