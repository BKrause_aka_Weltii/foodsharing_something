from __future__ import annotations

from abc import abstractmethod
from typing import Optional

from app.events.base import BaseEvent
from app.events.return_room_not_found import ReturnRoomNotFoundEvent
from app.room import Room
from app.room_factory import RoomFactory


class RoomProcessableEvent(BaseEvent):
    room_id: str

    def handle(self, room_factory: RoomFactory) -> Optional[BaseEvent]:
        room = room_factory.get_room(self.room_id)
        if room is None:
            return ReturnRoomNotFoundEvent(room_id=self.room_id)
        else:
            return self.process_room(room, room_factory)


    @abstractmethod
    def process_room(self, room: Room, room_factory: RoomFactory):
        pass
