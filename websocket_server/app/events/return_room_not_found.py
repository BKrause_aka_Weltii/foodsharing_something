from app.events.base import BaseEvent
from app.events.events import EventType


class ReturnRoomNotFoundEvent(BaseEvent):
    event_type=EventType.RETURN_ROOM_NOT_FOUND
    room_id: str
