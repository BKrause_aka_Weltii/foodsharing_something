from app.events.base import BaseEvent
from app.events.events import EventType
from app.room import Room


class InitialRoomStateEvent(BaseEvent):
    event_type = EventType.RETURN_INITIAL_ROOM_STATE
    room: Room
