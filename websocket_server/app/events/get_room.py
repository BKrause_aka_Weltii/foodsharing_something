from typing import Optional

from app.events.base import BaseEvent
from app.events.events import EventType
from app.events.return_get_room import ReturnGetRoomEvent
from app.events.return_room_not_found import ReturnRoomNotFoundEvent
from app.room_factory import RoomFactory


class GetRoomEvent(BaseEvent):
    event_type = EventType.GET_ROOM
    room_id: str

    def handle(self, room_factory: RoomFactory) -> Optional[BaseEvent]:
        room = room_factory.get_room(self.room_id)
        if room:
            return ReturnGetRoomEvent(room=room)
        else:
            return ReturnRoomNotFoundEvent(room_id=self.room_id)
