from app.room import Vector2
from app.events.base import BaseEvent
from app.events.events import EventType


class ReturnAddVectorToUser(BaseEvent):
    event_type = EventType.RETURN_ADD_VECTOR_TO_USER
    broadcast = True
    username: str
    vector: Vector2
