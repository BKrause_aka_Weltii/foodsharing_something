from app.events.add_vector_to_user.return_add_vector_to_user import ReturnAddVectorToUser
from app.events.return_user_not_found import ReturnUserNotFound
from app.events.room_processable import RoomProcessableEvent
from app.room import Vector2, Room, User
from app.room_factory import RoomFactory
from app.events.events import EventType


class AddVectorToUserEvent(RoomProcessableEvent):
    event_type = EventType.RETURN_ADD_VECTOR_TO_USER
    username: str
    vector: Vector2

    def process_room(self, room: Room, room_factory: RoomFactory):
        user: User = room.get_user(self.username)
        if not user:
            return ReturnUserNotFound(
                username=self.username
            )

        user.vectors.append(self.vector)
        room_factory.update_room(room)

        return ReturnAddVectorToUser(
            username=self.username,
            vector=self.vector
        )
