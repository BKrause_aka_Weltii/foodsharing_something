from app.room import Vector2
from app.events.base import BaseEvent
from app.events.events import EventType


class ReturnRemoveVectorFromUserEvent(BaseEvent):
    event_type = EventType.RETURN_REMOVE_VECTOR_FROM_USER
    broadcast = True
    username: str
    vector: Vector2
