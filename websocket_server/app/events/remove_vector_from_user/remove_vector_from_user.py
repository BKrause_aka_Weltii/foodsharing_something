from pydantic import BaseModel

from app.events.events import EventType
from app.events.remove_vector_from_user.return_remove_vector_from_user import ReturnRemoveVectorFromUserEvent
from app.events.return_user_not_found import ReturnUserNotFound
from app.events.room_processable import RoomProcessableEvent
from app.room import Vector2, Room, User
from app.room_factory import RoomFactory


class RemoveVectorFromUserEvent(RoomProcessableEvent):
    event_type: EventType = EventType.REMOVE_VECTOR_FROM_USER
    room_id: str
    username: str
    vector: Vector2

    def process_room(self, room: Room, room_factory: RoomFactory):
        user: User = room.get_user(self.username)
        if not user:
            return ReturnUserNotFound(
                username=self.username
            )
#
        user.vectors.remove(self.vector)
        room_factory.update_room(room)
#
        return ReturnRemoveVectorFromUserEvent(
            username=self.username,
            vector=self.vector
        )
