from app.events.base import BaseEvent
from app.events.events import EventType


class ReturnRoomHasUserEvent(BaseEvent):
    event_type = EventType.RETURN_ROOM_HAS_USER
    username: str
