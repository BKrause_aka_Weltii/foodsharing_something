from app.events.base import BaseEvent
from app.events.events import EventType
from app.room import User


class ReturnNewUserEvent(BaseEvent):
    event_type = EventType.RETURN_NEW_USER
    user: User
    broadcast = True
