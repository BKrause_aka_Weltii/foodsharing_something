from app.events.add_user_to_room.return_new_user import ReturnNewUserEvent
from app.events.add_user_to_room.return_room_has_user import ReturnRoomHasUserEvent
from app.events.events import EventType
from app.events.room_processable import RoomProcessableEvent
from app.room import Room
from app.room_factory import RoomFactory


class AddUserToRoomEvent(RoomProcessableEvent):
    event_type = EventType.ADD_USER_TO_ROOM
    username: str

    def process_room(self, room: Room, room_factory: RoomFactory):
        user = room.create_and_add_new_user(self.username)

        if user is None:
            return ReturnRoomHasUserEvent(username=self.username)
        else:
            room_factory.update_room(room)
            return ReturnNewUserEvent(user=user)
