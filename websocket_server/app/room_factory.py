from typing import Optional

from app.database.database_handler import DatabaseHandler
from app.room import Room, RoomDto


class RoomFactory:
    database_handler: DatabaseHandler

    def __init__(self, database_handler: DatabaseHandler):
        self.database_handler = database_handler

    def room_exists(self, id: str) -> bool:
        return self.database_handler.get_room(id) is not None

    def get_room(self, id: str) -> Optional[Room]:
        raw_room_data = self.database_handler.get_room(id)
        if raw_room_data is not None:
            room = Room(**raw_room_data)
            return room

    def create_room(self, room_dto: RoomDto) -> Optional[Room]:
        new_id = self.database_handler.get_unused_uuid()
        room = Room(id=new_id, **room_dto.dict())
        self.database_handler.add_room(room)
        return room

    def update_room(self, room: Room):
        self.database_handler.patch_room(room)
