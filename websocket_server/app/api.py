import json
from json import (
    JSONDecodeError,
)

from dependency_injector.errors import (
    NoSuchProviderError,
)
from fastapi import FastAPI, WebSocket
from pymongo.errors import ServerSelectionTimeoutError
from starlette.middleware.cors import CORSMiddleware
from starlette.websockets import WebSocketDisconnect

from app.container import (
    Container,
)
from app.events.initial_room_state import InitialRoomStateEvent
from app.room import RoomDto, Room
from app.session_manager import SessionManager, RoomNotAvailable


class App(FastAPI):
    def __init__(
        self,
    ):
        super().__init__()
        self.container = Container()
        self.session_manager = SessionManager(self.container.room_factory())

        try:
            print("Try to connect to mongo db")
            self.container.database_handler().db_client.server_info()
            print("Connected successfully to mongo! 🥳")
        except ServerSelectionTimeoutError as timeoutError:
            print("Cant connect to mongo db! Please ensure that the database is available.")
            raise timeoutError


app = App()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/room")
def create_room(room_dto: RoomDto) -> Room or None:
    return app.container.room_factory().create_room(room_dto)


@app.get("/room/{room_id}")
def get_rooms(room_id: str):
    return app.container.room_factory().get_room(room_id)


@app.websocket("/room/{room_id}/ws")
async def websocket_endpoint(websocket: WebSocket, room_id: str):
    try:
        room_session = app.session_manager.get_room_session(room_id)
        await room_session.connect(websocket)
        await websocket.send_json(
            InitialRoomStateEvent(
                room=app.container.room_factory().get_room(room_session.room_id)
            ).dict()
        )
        while True:
            # TODO A room handling is missing to broadcast to specific set of users!
            message = await websocket.receive_text()

            try:
                raw_event: dict = json.loads(message)
                if "event_type" in raw_event:
                    event_type = raw_event["event_type"]
                elif "eventType" in raw_event:
                    event_type = raw_event["eventType"]
                else:
                    await websocket.send_text("Missing event type!")
                    return

                # print(f"Process {event_type}")
                event = app.container.event_factory(
                    event_type,
                    **raw_event,
                )
                result_event = app.container.event_handler().handle_event(event)
                if result_event:
                    if result_event.broadcast:
                        await room_session.broadcast(result_event)
                    else:
                        await websocket.send_json(result_event.dict())
            except JSONDecodeError:
                print(message)
                await websocket.send_text(f"echo: {message}")
            except NoSuchProviderError as error:
                if raw_event:
                    await websocket.send_text(
                        f"Event {raw_event['event_type'] if raw_event else 'Unknown'} is not available"
                    )
                else:
                    await websocket.send_text("Invalid message!")
            except WebSocketDisconnect:
                await room_session.disconnect(websocket)
    except RoomNotAvailable as e:
        await websocket.send_text("Room not available!")
    except WebSocketDisconnect:
        await room_session.disconnect(websocket)
