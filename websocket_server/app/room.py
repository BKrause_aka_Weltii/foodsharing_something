from __future__ import (
    annotations,
)

import random
from typing import (
    List,
    Optional,
    Dict,
)

from pydantic import (
    BaseModel,
)


class Vector2(BaseModel):
    x: float
    y: float

    def __eq__(
        self,
        other: Vector2,
    ):
        return self.x == other.x and self.y == other.y


class User(BaseModel):
    username: str
    color: str
    vectors: List[Vector2]


class RoomDto(BaseModel):
    name: str
    password: Optional[str]


class Room(BaseModel):
    id: str
    name: str
    users: Dict[
        str,
        User,
    ] = []

    def has_user(
        self,
        name: str,
    ):
        return name in self.users

    def get_next_user_color(self) -> str:
        return random.choice(
            ["red", "green", "blue", "pink", "yellow", "lightgray", "orange", "limegreen"]
        )

    def create_and_add_new_user(self, username: str) -> Optional[User]:
        if self.has_user(username):
            return None

        user = User(username=username, color=self.get_next_user_color(), vectors=[])
        self.users[username] = user
        return user

    def remove_user(self, username: str):
        if self.has_user(username):
            self.users.pop(username)

    def get_user(self, username) -> Optional[User]:
        return self.users.get(username, None)
