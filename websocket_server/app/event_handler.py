from typing import Optional

from app.events.base import BaseEvent
from app.room_factory import RoomFactory


class EventHandler:
    room_factory: RoomFactory

    def __init__(self, room_factory: RoomFactory):
        self.room_factory = room_factory

    def handle_event(self, event: BaseEvent) -> Optional[BaseEvent]:
        return event.handle(self.room_factory)
