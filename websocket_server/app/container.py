from dependency_injector import (
    containers,
    providers,
)

from app.events.add_vector_to_user.add_vector_to_user import AddVectorToUserEvent
from app.events.remove_user_from_room.remove_user_from_room import (
    RemoveUserFromRoomEvent,
)
from app.database.database_handler import DatabaseHandler
from app.event_handler import (
    EventHandler,
)
from app.events.remove_vector_from_user.remove_vector_from_user import (
    RemoveVectorFromUserEvent,
)
from app.events.add_user_to_room.add_user_to_room import (
    AddUserToRoomEvent,
)
from app.events.events import EventType
from app.events.get_room import GetRoomEvent
from app.room_factory import RoomFactory


class Container(containers.DeclarativeContainer):
    available_events = dict()
    available_events[EventType.GET_ROOM.value] = providers.Factory(GetRoomEvent)
    available_events[EventType.ADD_USER_TO_ROOM.value] = providers.Factory(
        AddUserToRoomEvent
    )
    available_events[EventType.REMOVE_USER_FROM_ROOM.value] = providers.Factory(
        RemoveUserFromRoomEvent
    )
    available_events[EventType.ADD_VECTOR_TO_USER.value] = providers.Factory(
        AddVectorToUserEvent
    )
    available_events[EventType.REMOVE_VECTOR_FROM_USER.value] = providers.Factory(
        RemoveVectorFromUserEvent
    )
    event_factory = providers.FactoryAggregate(**available_events)

    database_handler = providers.Singleton(DatabaseHandler)

    # TODO check if the dependency_injector has something similar
    room_factory = providers.Singleton(RoomFactory, database_handler=database_handler)

    event_handler = providers.Singleton(EventHandler, room_factory=room_factory)
