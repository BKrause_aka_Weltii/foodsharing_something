import os
import uuid
from typing import Any, Optional

from pymongo import MongoClient
from pymongo.results import InsertOneResult

from app.room import Room

MONGO_HOST = os.getenv("MONGO_HOST")
MONGO_PORT = int(os.getenv("MONGO_PORT"))
MONGO_USERNAME = os.getenv("MONGO_USERNAME")
MONGO_PASSWORD = os.getenv("MONGO_PASSWORD")
MONGO_DATABASE = os.getenv("MONGO_DATABASE")

MONGO_DATABASE_COLLECTION = "rooms"


class DatabaseHandler:
    def __init__(self, mongo_client: Optional[Any] = None):
        self.db_client = mongo_client if mongo_client else MongoClient(
            MONGO_HOST,
            MONGO_PORT,
            username=MONGO_USERNAME,
            password=MONGO_PASSWORD
        )
        self.db = self.db_client.get_database(MONGO_DATABASE)
        self.rooms = self.db.get_collection(MONGO_DATABASE_COLLECTION)

    def get_room(self, id: str) -> Any or None:
        data = self.rooms.find_one(dict(
            id=id
        ))
        return data

    def add_room(self, room: Room) -> InsertOneResult:
        return self.rooms.insert_one(room.dict())

    def get_unused_uuid(self) -> str:
        id = uuid.uuid4().__str__()
        if self.get_room(id):
            return self.get_unused_uuid()
        return id

    def patch_room(self, room: Room):
        self.rooms.replace_one(dict(id=room.id), room.dict())
